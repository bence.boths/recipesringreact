package edu.codespring.recipe.backend.service;

import edu.codespring.recipe.backend.model.Comment;
import edu.codespring.recipe.backend.model.Direction;

import java.util.List;

public interface DirectionService {

    void addDirection(Direction direction);
    void deleteDirection(int did);
    Iterable<Direction> getAllDirection(int id);
    void deleteAllByRid(int rid);
}
