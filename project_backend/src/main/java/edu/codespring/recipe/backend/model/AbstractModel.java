package edu.codespring.recipe.backend.model;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Objects;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractModel {

	private String uuid;

	@PrePersist
	public void generateUuid() {
		uuid = UUID.randomUUID().toString();
	}

	public String getUuid() {
		if (uuid == null) {
			generateUuid();
		}
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AbstractModel that = (AbstractModel) o;
		return Objects.equals(getUuid(), that.getUuid());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getUuid());
	}

}
