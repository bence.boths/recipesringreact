package edu.codespring.recipe.backend.repository.memory;

import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.UserDAO;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Profile("!jdbc")
public class MemoryUserDAO implements UserDAO {

	private ConcurrentMap<Long, User> users;
	private AtomicLong idCount;

	public MemoryUserDAO() {
		users = new ConcurrentHashMap<>();
		idCount = new AtomicLong();
	}

	@Override
	public User getByUserName(String userName) {
		throw new RepositoryException("Not implemented in memory user access.");
	}

}
