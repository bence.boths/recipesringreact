package edu.codespring.recipe.backend.repository.springdata;

import edu.codespring.recipe.backend.model.Direction;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Profile("jdbc")
public interface SpringDataJpaDirectionDAO extends CrudRepository<Direction, Integer> {

//    @Query("FROM directions WHERE rid = :rid")
    Iterable<Direction> findAllByRid(int rid);

    @Transactional
    void deleteAllByRid(int rid);
}
