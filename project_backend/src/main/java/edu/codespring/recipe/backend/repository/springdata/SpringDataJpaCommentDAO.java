package edu.codespring.recipe.backend.repository.springdata;

import edu.codespring.recipe.backend.model.Comment;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Profile("jdbc")
public interface SpringDataJpaCommentDAO extends CrudRepository<Comment, Integer> {

//    @Query("DELETE FROM comments WHERE rid = :rid")
//    @Modifying
    @Transactional
    void deleteCommentByRid( int rid);
//    @Query("FROM comments WHERE rid = :rid")
    Iterable<Comment> findAllByRid(int rid);
//    @Query("FROM comments WHERE id = :id")
    Iterable<Comment> findAllById(long id);
}
