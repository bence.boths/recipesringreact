package edu.codespring.recipe.backend.controller;

import edu.codespring.recipe.backend.model.Direction;
import edu.codespring.recipe.backend.service.DirectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@Transactional
@RequestMapping("/api/directions")
public class DirectionController {

    @Autowired
    private DirectionService directionService;

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public void addDirection(@RequestBody Direction direction) {
        directionService.addDirection(direction);
    }

    @CrossOrigin
    @DeleteMapping("/{did}")
    @ResponseBody
    public void deleteDirection(@PathVariable int did) {
        directionService.deleteDirection(did);
    }

    @CrossOrigin
    @GetMapping("/{rid}")
    @ResponseBody
    public Iterable<Direction> getAllDirection(@PathVariable int rid) {
        return directionService.getAllDirection(rid);
    }

    @CrossOrigin
    @DeleteMapping("/rid/{rid}")
    @ResponseBody
    public void deleteAllByRid(@PathVariable int rid) {
        directionService.deleteAllByRid(rid);
    }
}
