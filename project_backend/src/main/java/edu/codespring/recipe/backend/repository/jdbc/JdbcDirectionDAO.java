package edu.codespring.recipe.backend.repository.jdbc;

import edu.codespring.recipe.backend.model.Direction;
import edu.codespring.recipe.backend.repository.DirectionDAO;
import edu.codespring.recipe.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("jdbc")
public class JdbcDirectionDAO implements DirectionDAO {

    private final Logger LOG = LoggerFactory.getLogger(JdbcDirectionDAO.class);

    @Autowired
    private ConnectionManager connectionManager;

}
