package edu.codespring.recipe.backend.controller;

import edu.codespring.recipe.backend.model.Comment;
import edu.codespring.recipe.backend.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/api/comments")
public class CommentController {

    @Autowired
    CommentService commentService;

    @CrossOrigin
    @GetMapping("/id/{id}")
    @ResponseBody
    public Iterable<Comment> getAllCommentById(@PathVariable int id) {
        return commentService.getAllCommentByUser(id);
    }

    @CrossOrigin
    @GetMapping("/rid/{rid}")
    @ResponseBody
    public Iterable<Comment> getAllCommentByRid(@PathVariable int rid) {
        return commentService.getAllCommentByRecipe(rid);
    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public void createComment(@RequestBody Comment comment) {
        commentService.createComment(comment);
    }

    @CrossOrigin
    @DeleteMapping("/{cid}")
    @ResponseBody
    public void deleteComment(@PathVariable int cid) {
        commentService.deleteComment(cid);
    }

    @CrossOrigin
    @DeleteMapping("/rid/{rid}")
    @ResponseBody
    public void deleteCommentByRid(@PathVariable int rid) {
        commentService.deleteCommentByRecipe(rid);
    }

    @CrossOrigin
    @GetMapping("/{cid}")
    @ResponseBody
    public Comment getCommentById(@PathVariable int cid) {
        return commentService.getCommentById(cid);
    }
}
