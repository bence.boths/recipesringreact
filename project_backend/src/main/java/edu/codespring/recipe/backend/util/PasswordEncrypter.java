package edu.codespring.recipe.backend.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class PasswordEncrypter {

	private static final Logger LOG = LoggerFactory.getLogger(PasswordEncrypter.class);

	@Value("${password_algorithm}")
	private String passwordAlgorithm;

	@Value("${password_encoding}")
	private String passwordEncoding;


	public String generateHashedPassword(String plainPassword, String plainSalt) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		try {
			MessageDigest algorithm = MessageDigest.getInstance(passwordAlgorithm);
			byte[] bytes = (plainPassword + plainSalt).getBytes(passwordEncoding);

			algorithm.reset();
			algorithm.update(bytes);

			return toHexString(algorithm.digest());
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			LOG.error("Failed to generate hashed password; " + e.getMessage());
			throw e;
		}
	}

	private String toHexString(byte[] bytes) {
		StringBuilder buffer = new StringBuilder();

		for (byte i : bytes) {
			String hex = Integer.toHexString(0xFF & i);

			if (hex.length() == 1) {
				buffer.append('0');
			}

			buffer.append(hex);
		}

		return buffer.toString();
	}

}
