package edu.codespring.recipe.backend.service.impl;

import edu.codespring.recipe.backend.model.Ingredients;
import edu.codespring.recipe.backend.repository.IngredientsDAO;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.springdata.SpringDataJpaIngredientsDAO;
import edu.codespring.recipe.backend.service.IngredientService;
import edu.codespring.recipe.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientsServiceImpl implements IngredientService {

    private final Logger LOG = LoggerFactory.getLogger(IngredientsServiceImpl.class);

    @Autowired
    private SpringDataJpaIngredientsDAO dao;

    @Override
    public void addIngredients(Ingredients ingredients) {
        try {
            dao.save(ingredients);
            LOG.info("Successfully created ingredient ");
        } catch (RepositoryException e) {
            LOG.error("Failed to add ingredient! " + e.getMessage());
            throw new ServiceException("Failed to add ingredient! " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteIngredients(int iid) {
        try {
            dao.deleteById(iid);
            LOG.info("Successfully deleted ingredient!");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete ingredient by id " + iid + "; " + e.getMessage());
            throw new ServiceException("Failed to delete ingredient by id " + iid + "; " + e.getMessage(), e);
        }
    }

    @Override
    public Iterable<Ingredients> getAllIngredients(int rid) {
        try {
            Iterable<Ingredients> ingredients = dao.findAllByRid(rid);
            LOG.info("Successfully queried ingredients.");
            return ingredients;
        } catch (RepositoryException e) {
            LOG.error("Failed to query all ingredient! " + e.getMessage());
            throw new ServiceException("Failed to query all ingredient! " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteAllByRid(int rid) {
        try {
            dao.deleteAllByRid(rid);
            LOG.info("Successfully deleted ingredients.");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete all ingredient! " + e.getMessage());
            throw new ServiceException("Failed to delete all ingredient! " + e.getMessage(), e);
        }
    }
}
