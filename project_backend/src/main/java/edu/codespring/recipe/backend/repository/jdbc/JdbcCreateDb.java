package edu.codespring.recipe.backend.repository.jdbc;

import edu.codespring.recipe.backend.model.Recipe;
import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.service.RecipeService;
import edu.codespring.recipe.backend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
@Profile("jdbc")
public class JdbcCreateDb {

    private final Logger LOG = LoggerFactory.getLogger(JdbcCreateDb.class);

    @Autowired
    UserService us;

    @Autowired
    RecipeService rs;

    @Autowired
    private ConnectionManager connectionManager;

    @Value("${jdbc_create_tables}")
    private boolean createTables;

    public void createDb() {
        if (createTables) {
            LOG.info("Creating tables");

            Connection c = null;
            try {
                c = connectionManager.getConnection();
                Statement statement = c.createStatement();
                statement.executeUpdate("create table users (id int auto_increment primary key,uuid varchar(50) not null,username varchar(30),password varchar(50));");
                statement.executeUpdate("create table recipes (rid int auto_increment primary key, name varchar(50), image varchar(50), time int, difficulty int, id int, constraint fk_recipe_id foreign key(id) references users(id));");
                statement.executeUpdate("create table ingredients (iid int auto_increment primary key, name varchar(30), quantity int, rid int, constraint fk_ing_rec foreign key (rid) references recipes(rid));");
                statement.executeUpdate("create table directions (did int auto_increment primary key, description varchar(300), rid int, constraint fk_dir_rec foreign key (rid) references recipes(rid));");
                statement.executeUpdate("create table comments (cid int auto_increment primary key, text varchar(300), date date, id int, rid int, constraint fk_user foreign key(id) references users(id), constraint fk_recipe foreign key(rid) references recipes(rid));");
                LOG.info("Table created successfully");

            } catch (SQLException e) {
                LOG.error("Failed to create table.", e);
                throw new RepositoryException("Failed to create table.", e);
            } finally {
                if (c != null) {
                    LOG.info("Return connections!");
                    connectionManager.returnConnection(c);
                }
            }
        }
    }

//    public void insertData() {
//        User user = new User();
//        user.setUsername("Bence");
//        user.setPassword("Valami");
//        us.register(user);
//
//        Recipe recipe = new Recipe();
//        recipe.setRid(1);
//        recipe.setId(1);
//        recipe.setName("Gulyas");
//        recipe.setDifficulty(3);
//        recipe.setTime(4);
//        recipe.setImg("images/gulyas.jpg");
//        Recipe recipe2 = new Recipe();
//        recipe2.setRid(2);
//        recipe2.setId(1);
//        recipe2.setName("Gulyas2");
//        recipe2.setDifficulty(3);
//        recipe2.setTime(4);
//        recipe2.setImg("images/gulyas2.jpg");
//        rs.addRecipe(recipe,1);
//        rs.addRecipe(recipe2,1);
//
//    }
}
