package edu.codespring.recipe.backend.repository.springdata;

import edu.codespring.recipe.backend.model.Recipe;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.servlet.http.Part;


@Repository
@Profile("jdbc")
public interface SpringDataJpaRecipeDAO extends CrudRepository<Recipe, Integer> {

    Recipe getRecipeByName(String name);
}
