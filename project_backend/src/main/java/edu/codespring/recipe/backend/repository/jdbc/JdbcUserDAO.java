package edu.codespring.recipe.backend.repository.jdbc;

import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("jdbc")
public class JdbcUserDAO implements UserDAO {

	private static final Logger LOG = LoggerFactory.getLogger(JdbcUserDAO.class);

	@Autowired
	private ConnectionManager connManager;

	@Override
	public User getByUserName(String userName) {
		Connection c = null;
		User u = null;
		try {
			c = connManager.getConnection();
			PreparedStatement prepStmt = c.prepareStatement("SELECT * FROM users WHERE username = ?;");
			prepStmt.setString(1, userName);

			ResultSet rs = prepStmt.executeQuery();
			if (!rs.next()) {
				LOG.info("Non-existing user " + userName);
				return null;
			}
			u = new User();
			u.setId(rs.getLong("id"));
			u.setUuid(rs.getString("uuid"));
			u.setUsername(rs.getString("userName"));
			u.setPassword(rs.getString("password"));
		} catch (SQLException | RepositoryException e) {
			LOG.error("Failed to query User.", e);
			throw new RepositoryException("Failed to query User.", e);
		} finally {
			if (c != null) {
				LOG.info("Return connection!");
				connManager.returnConnection(c);
			}
		}
		return u;
	}

}
