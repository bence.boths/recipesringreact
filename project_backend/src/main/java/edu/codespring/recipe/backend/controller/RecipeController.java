package edu.codespring.recipe.backend.controller;

import edu.codespring.recipe.backend.model.Recipe;
import edu.codespring.recipe.backend.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Part;
import javax.transaction.Transactional;
import java.awt.*;

@Controller
@RequestMapping("/api/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public Recipe createRecipe(@RequestBody Recipe recipe) {
        return recipeService.addRecipe(recipe);
    }

    @CrossOrigin
    @PostMapping("/upload")
    @ResponseBody
    public void uploadImage(@RequestParam("imageFile") MultipartFile file, @RequestParam("imageName") String name) {
        recipeService.uploadImage(file);
    }

    @CrossOrigin
    @DeleteMapping("/{rid}")
    @ResponseBody
    public void deleteRecipe(@PathVariable int rid) {
        recipeService.deleteRecipe(rid);
    }

    @CrossOrigin
    @GetMapping
    @ResponseBody
    public Iterable<Recipe> getAllRecipes() {
        return recipeService.getAllRecipe();
    }

    @CrossOrigin
    @GetMapping("/{rid}")
    @ResponseBody
    public Recipe getRecipeById(@PathVariable int rid) {
        return recipeService.getRecipeById(rid);
    }

    @CrossOrigin
    @GetMapping("/name/{name}")
    @ResponseBody
    public Recipe getRecipeByName(@PathVariable String name) {
        return recipeService.getRecipeByName(name);
    }

}
