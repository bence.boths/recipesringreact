package edu.codespring.recipe.backend.service.impl;

import edu.codespring.recipe.backend.model.Direction;
import edu.codespring.recipe.backend.repository.DirectionDAO;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.springdata.SpringDataJpaDirectionDAO;
import edu.codespring.recipe.backend.service.DirectionService;
import edu.codespring.recipe.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectionServiceImpl implements DirectionService {

    private final Logger LOG = LoggerFactory.getLogger(DirectionServiceImpl.class);

    @Autowired
    private SpringDataJpaDirectionDAO dao;

    @Override
    public void addDirection(Direction direction) {
        try {
            dao.save(direction);
            LOG.info("Successfully created direction!");
        } catch (RepositoryException e) {
            LOG.error("Failed to add direction! " + e.getMessage());
            throw new ServiceException("Failed to add direction! " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteDirection(int did) {
        try {
            dao.deleteById(did);
            LOG.info("Successfully deleted direction!");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete direction by id " + did + "; " + e.getMessage());
            throw new ServiceException("Failed to delete direction by id " + did + "; " + e.getMessage(), e);
        }
    }

    @Override
    public Iterable<Direction> getAllDirection(int rid) {
        try {
            Iterable<Direction> directions = dao.findAllByRid(rid);
            LOG.info("Successfully queried direction.");
            return directions;
        } catch (RepositoryException e) {
            LOG.error("Failed to query all direction! " + e.getMessage());
            throw new ServiceException("Failed to query all direction! " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteAllByRid(int rid) {
        try {
            dao.deleteAllByRid(rid);
            LOG.info("Successfully deleted direction.");
        } catch (RepositoryException e) {
            LOG.error("Failed to deleted all direction! " + e.getMessage());
            throw new ServiceException("Failed to deleted all direction! " + e.getMessage(), e);
        }
    }
}
