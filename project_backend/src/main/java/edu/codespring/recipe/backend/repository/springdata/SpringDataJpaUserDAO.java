package edu.codespring.recipe.backend.repository.springdata;

import edu.codespring.recipe.backend.model.User;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jdbc")
public interface SpringDataJpaUserDAO extends CrudRepository<User, Long> {

//    @Query("from users where username = :username ")
    User findByUsername(/*@Param("username")*/ String username);
}
