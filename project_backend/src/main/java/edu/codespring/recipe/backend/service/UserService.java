package edu.codespring.recipe.backend.service;

import edu.codespring.recipe.backend.model.User;

import java.util.List;

public interface UserService {

	void register(User user);
	boolean login(User user);
	void delete(Long id);
	User getById(Long id);
	Iterable<User> getAll();
	void update(User user);
	User getByName(String username);
}
