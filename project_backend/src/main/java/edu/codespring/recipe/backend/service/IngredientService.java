package edu.codespring.recipe.backend.service;

import edu.codespring.recipe.backend.model.Ingredients;

import java.util.List;

public interface IngredientService {

    void addIngredients(Ingredients ingredients);
    void deleteIngredients(int iid);
    Iterable<Ingredients> getAllIngredients(int rid);
    void deleteAllByRid(int rid);
}
