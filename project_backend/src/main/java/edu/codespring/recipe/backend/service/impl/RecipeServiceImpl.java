package edu.codespring.recipe.backend.service.impl;

import edu.codespring.recipe.backend.model.Recipe;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.springdata.SpringDataJpaRecipeDAO;
import edu.codespring.recipe.backend.service.RecipeService;
import edu.codespring.recipe.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
public class RecipeServiceImpl implements RecipeService {

    private static final Logger LOG = LoggerFactory.getLogger(RecipeServiceImpl.class);

    @Autowired
    private SpringDataJpaRecipeDAO dao;

    @Override
    public Recipe addRecipe(Recipe recipe) {
        try {
            LOG.info("Successfully created recipe!");
            return dao.save(recipe);
        } catch (RepositoryException e) {
            LOG.error("Failed to add recipe! " + e.getMessage());
            throw new ServiceException("Failed to add recipe! " + e.getMessage(), e);
        }
    }

    @Override
    public void uploadImage(MultipartFile file) {

    }

    @Override
    public void deleteRecipe(int rid) {
        try {
            dao.deleteById(rid);
            LOG.info("Successfully deleted recipe !");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete recipe by id " + rid + "; " + e.getMessage());
            throw new ServiceException("Failed to delete recipe by id " + rid + "; " + e.getMessage(), e);
        }
    }

    @Override
    public Iterable<Recipe> getAllRecipe() {
        try {
            Iterable<Recipe> recipes = dao.findAll();
            LOG.info("Successfully queried recipes.");
            return recipes;
        } catch (RepositoryException e) {
            LOG.error("Failed to query all recipes! " + e.getMessage());
            throw new ServiceException("Failed to query all recipes! " + e.getMessage(), e);
        }
    }

    @Override
    public Recipe getRecipeById(int rid) {
        try {
            Optional<Recipe> optional = dao.findById(rid);
            if(optional.isPresent()) {
                LOG.info("Successfully queried recipe.");
                return optional.get();
            } else {
                LOG.info("Unsuccesfull queried recipe");
                return null;
            }
        } catch (RepositoryException e) {
            LOG.error("Failed to query the recipe! " + e.getMessage());
            throw new ServiceException("Failed to query the recipe! " + e.getMessage(), e);
        }
    }

    @Override
    public Recipe getRecipeByName(String name) {
        try {
            Recipe recipe = dao.getRecipeByName(name);
            LOG.info("Successfully queried recipe.");
            return recipe;
        } catch (RepositoryException e) {
            LOG.error("Failed to query the recipe! " + e.getMessage());
            throw new ServiceException("Failed to query the recipe! " + e.getMessage(), e);
        }
    }
}
