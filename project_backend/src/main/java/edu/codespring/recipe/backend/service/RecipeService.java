package edu.codespring.recipe.backend.service;

import edu.codespring.recipe.backend.model.Recipe;
import org.springframework.web.multipart.MultipartFile;

public interface RecipeService {

    Recipe addRecipe(Recipe recipe);
    void uploadImage(MultipartFile file);
    void deleteRecipe(int rid);
    Iterable<Recipe> getAllRecipe();
    Recipe getRecipeById(int rid);
    Recipe getRecipeByName(String name);
}
