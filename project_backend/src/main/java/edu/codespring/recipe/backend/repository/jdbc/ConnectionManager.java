package edu.codespring.recipe.backend.repository.jdbc;

import edu.codespring.recipe.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Repository
@Profile("jdbc")
class ConnectionManager {

	private static final Logger LOG = LoggerFactory.getLogger(ConnectionManager.class);

	private List<Connection> pool;

	@Value("${jdbc_pool_size}")
	private int poolSize;
	@Value("${jdbc_driver}")
	private String jdbcDriver;
	@Value("${jdbc_url}")
	private String jdbcUrl;
	@Value("${jdbc_user}")
	private String jdbcUser;
	@Value("${jdbc_password}")
	private String jdbcPassword;

	@PostConstruct
	private void  buildConnectionManager() {

		try {
			Class.forName(jdbcDriver);
			pool = new LinkedList<>();
			for (int c = 0; c < poolSize; ++c) {
				pool.add(DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword));
			}
			LOG.info("Successfully initialized ConnectionManager and filled pool with {} connections.", poolSize);
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Failed to initialize ConnectionManager.", e);
			throw new RepositoryException("Failed to initialize ConnectionManager.", e);
		}
	}

	public synchronized Connection getConnection() throws RepositoryException {
		Connection c = null;

		if (pool.size() > 0) {
			c = pool.get(0);
			pool.remove(0);
		}

		if (c == null) {
			throw new RepositoryException("Empty connection pool.");
		}

		return c;
	}

	public synchronized void returnConnection(Connection c) {
		if (pool.size() < poolSize) {
			pool.add(c);
		}
	}

}
