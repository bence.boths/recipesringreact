package edu.codespring.recipe.backend;

import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@ComponentScan
@Configuration
@EnableAutoConfiguration
public class Main {

//    @Autowired
//    private UserService us;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @PostConstruct
    public void buildMain() {
//        User user = new User();
//        user.setUsername("Test2");
//        user.setPassword("Test2");
//        us.register(user);
//        System.out.println("register...");
//        System.out.println(us.login(user));
    }
}
