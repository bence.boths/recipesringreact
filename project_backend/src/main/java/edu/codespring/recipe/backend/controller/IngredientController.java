package edu.codespring.recipe.backend.controller;

import edu.codespring.recipe.backend.model.Ingredients;
import edu.codespring.recipe.backend.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@Transactional
@RequestMapping("/api/ingredients")
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public void addIngredient(@RequestBody Ingredients ingredients) {
        ingredientService.addIngredients(ingredients);
    }

    @CrossOrigin
    @DeleteMapping("/{iid}")
    @ResponseBody
    public void deleteIngredient(@PathVariable int iid) {
        ingredientService.deleteIngredients(iid);
    }

    @CrossOrigin
    @GetMapping("/{rid}")
    @ResponseBody
    public Iterable<Ingredients> getAllIngredient(@PathVariable int rid) {
        return ingredientService.getAllIngredients(rid);
    }

    @CrossOrigin
    @DeleteMapping("/rid/{rid}")
    @ResponseBody
    public void deleteAllByRid(@PathVariable int rid) {
        ingredientService.deleteAllByRid(rid);
    }
}
