package edu.codespring.recipe.backend.service;

import edu.codespring.recipe.backend.model.Comment;

import java.util.List;

public interface CommentService {

    void createComment(Comment comment);
    void deleteComment(int cid);
    void deleteCommentByRecipe(int rid);
    Comment getCommentById(int cid);
    Iterable<Comment> getAllCommentByUser(int id);
    Iterable<Comment> getAllCommentByRecipe(int rid);
}
