package edu.codespring.recipe.backend.repository;

import edu.codespring.recipe.backend.model.User;

import java.util.List;

public interface UserDAO {
	User getByUserName(String userName);
}
