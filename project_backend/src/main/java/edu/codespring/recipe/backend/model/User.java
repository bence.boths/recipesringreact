package edu.codespring.recipe.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User extends BaseEntity {

	@Column(nullable = false, length = 127)
	private String username;
	@Column(nullable = false)
	private String password;

	public User() {}
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User{" +
						"username='" + username + '\'' +
						", password='" + password + '\'' +
						", id='" + getId() + '\'' +
						", uuid='" + getUuid() + '\'' +
						'}';
	}

}
