package edu.codespring.recipe.backend.repository.springdata;

import edu.codespring.recipe.backend.model.Ingredients;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Profile("jdbc")
public interface SpringDataJpaIngredientsDAO extends CrudRepository<Ingredients, Integer> {

//    @Query("FROM ingredients WHERE rid = :rid")
    Iterable<Ingredients> findAllByRid(int rid);
    @Transactional
    void deleteAllByRid(int rid);
}
