package edu.codespring.recipe.backend.service.impl;

import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.repository.springdata.SpringDataJpaUserDAO;
import edu.codespring.recipe.backend.service.ServiceException;
import edu.codespring.recipe.backend.service.UserService;
import edu.codespring.recipe.backend.util.PasswordEncrypter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private SpringDataJpaUserDAO dao;
	@Autowired
	private PasswordEncrypter passwordEncrypter;

	@Override
	public void register(User user) {
		try {
			if (dao.findByUsername(user.getUsername()) != null) {
				LOG.info("Failed to register user, user by username " + user.getUsername() + " already exists.");
				return;
			}

			user.setPassword(passwordEncrypter.generateHashedPassword(user.getPassword(), user.getUuid()));
			dao.save(user);
			LOG.info("Successfully created user with username " + user.getUsername());
		} catch (RepositoryException | UnsupportedEncodingException | NoSuchAlgorithmException e) {
			LOG.error("Failed to register user! " + e.getMessage());
			throw new ServiceException("Failed to register user! " + e.getMessage(), e);
		}
	}

	@Override
	public boolean login(User user) {
		User u = null;
		try {
			u = dao.findByUsername(user.getUsername());
			if (u == null) {
				LOG.info("Failed to login user " + user.getUsername());
				return false;
			}
			user.setPassword(passwordEncrypter.generateHashedPassword(user.getPassword(), u.getUuid()));
			if (u.getPassword().equals(user.getPassword())) {
				LOG.info("Successfully logged in user " + user.getUsername());
				return true;
			} else {
				LOG.info("Failed to login user " + user.getUsername());
				return false;
			}
		} catch (RepositoryException | UnsupportedEncodingException | NoSuchAlgorithmException e) {
			LOG.error("Failed to login user! " + e.getMessage());
			throw new ServiceException("Failed to login user! " + e.getMessage(), e);
		}
	}

	@Override
	public void delete(Long id) {
		try {
			dao.deleteById(id);
			LOG.info("Successfully deleted user!");
		} catch (RepositoryException e) {
			LOG.error("Failed to delete user by id " + id + "; " + e.getMessage());
			throw new ServiceException("Failed to delete user by id " + id + "; " + e.getMessage(), e);
		}
	}

	@Override
	public User getById(Long id) {
		try {
			if(id == null) {
				throw new ServiceException("Given id is null!");
			}

			Optional<User> optional = dao.findById(id);

			if(!optional.isPresent()) {
				LOG.info("Non-existing user with id " + id);
				return null;
			} else {
				LOG.info("Successfully queried user with id " + id);
				return optional.get();
			}
		} catch (RepositoryException e) {
			LOG.error("Failed to query user with id " + id + "; " + e.getMessage());
			throw new ServiceException("Failed to query user with id " + id + "; " + e.getMessage(), e);
		}
	}

	@Override
	public Iterable<User> getAll() {
		try {
			Iterable<User> users = dao.findAll();
			LOG.info("Successfully queried users.");
			return users;
		} catch (RepositoryException e) {
			LOG.error("Failed to query all users! " + e.getMessage());
			throw new ServiceException("Failed to query all users! " + e.getMessage(), e);
		}
	}

	@Override
	public void update(User user) {
		try {
			User u = dao.findByUsername(user.getUsername());
			if (u == null) {
				LOG.info("Failed to update user with username " + user.getUsername() + "; non-existing user.");
				return;
			}
			dao.save(user);
			LOG.info("Successfully updated user with username " + user.getUsername());
		} catch (RepositoryException e) {
			LOG.error("Failed to update user with username " + user.getUsername() + "; " + e.getMessage());
			throw new ServiceException("Failed to update user with username " + user.getUsername() + "; " + e.getMessage(), e);
		}
	}

	@Override
	public User getByName(String username) {
		try {
			User u = dao.findByUsername(username);
			if (u == null) {
				LOG.info("Non-existing user " + username);
			} else {
				LOG.info("Successfully queried user " + username);
			}
			return u;
		} catch (RepositoryException e) {
			LOG.error("Failed to query user " + username + "; " + e.getMessage());
			throw new ServiceException("Failed to query user " + username + "; " + e.getMessage(), e);
		}
	}
}
