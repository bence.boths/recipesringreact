package edu.codespring.recipe.backend.service.impl;

import edu.codespring.recipe.backend.model.Comment;
import edu.codespring.recipe.backend.repository.RepositoryException;
import edu.codespring.recipe.backend.repository.springdata.SpringDataJpaCommentDAO;
import edu.codespring.recipe.backend.service.CommentService;
import edu.codespring.recipe.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private final Logger LOG = LoggerFactory.getLogger(CommentServiceImpl.class);

    @Autowired
    private SpringDataJpaCommentDAO dao;

    @Override
    public void createComment(Comment comment) {
        try {
            dao.save(comment);
            LOG.info("Successfully created comment!");
        } catch (RepositoryException e) {
            LOG.error("Failed to add comment! " + e.getMessage());
            throw new ServiceException("Failed to add comment! " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteComment(int cid) {
        try {
            dao.deleteById(cid);
            LOG.info("Successfully deleted comment!");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete comment by id " + cid + "; " + e.getMessage());
            throw new ServiceException("Failed to delete comment by id " + cid + "; " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteCommentByRecipe(int rid) {
        try {
            dao.deleteCommentByRid(rid);
            LOG.info("Successfully deleted comment!");
        } catch (RepositoryException e) {
            LOG.error("Failed to delete comment by id " + rid + "; " + e.getMessage());
            throw new ServiceException("Failed to delete comment by id " + rid + "; " + e.getMessage(), e);
        }
    }

    @Override
    public Comment getCommentById(int cid) {
        try {
            Optional<Comment> optional = dao.findById(cid);
            if(!optional.isPresent()) {
                LOG.info("Non-existing comment with id " + cid);
                return null;
            } else {
                LOG.info("Successfully queried comment with id " + cid);
                return optional.get();
            }
        } catch (RepositoryException e) {
            LOG.error("Failed to get comment by id " + cid + "; " + e.getMessage());
            throw new ServiceException("Failed to get comment by id " + cid + "; " + e.getMessage(), e);
        }
    }

    @Override
    public Iterable<Comment> getAllCommentByUser(int id) {
        try {
            Iterable<Comment> comments = dao.findAllById(id);
            LOG.info("Successfully queried comments.");
            return comments;
        } catch (RepositoryException e) {
            LOG.error("Failed to query all comments! " + e.getMessage());
            throw new ServiceException("Failed to query all comments! " + e.getMessage(), e);
        }
    }

    @Override
    public Iterable<Comment> getAllCommentByRecipe(int rid) {
        try {
            Iterable<Comment> comments = dao.findAllByRid(rid);
            LOG.info("Successfully queried comments.");
            return comments;
        } catch (RepositoryException e) {
            LOG.error("Failed to query all comments! " + e.getMessage());
            throw new ServiceException("Failed to query all comments! " + e.getMessage(), e);
        }
    }
}
