package edu.codespring.recipe.backend.controller;

import edu.codespring.recipe.backend.model.User;
import edu.codespring.recipe.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @CrossOrigin
    @GetMapping
    @ResponseBody
    public Iterable<User> getAllUsers() {
        return userService.getAll();
    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public void createUser(@RequestBody User user) {
        userService.register(user);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    @ResponseBody
    public String findById(@PathVariable Long id) {
        User returnValue = userService.getById(id);
        return returnValue.getUsername();
    }

    @CrossOrigin
    @GetMapping("/name/{name}")
    @ResponseBody
    public long findByName(@PathVariable String name) {
        return userService.getByName(name).getId();
    }

    @CrossOrigin
    @PostMapping("/login")
    @ResponseBody
    public boolean loginUser(@RequestBody User user) {
        return userService.login(user);
    }
}
