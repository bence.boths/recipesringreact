import './App.css';
import Nav from './components/Nav';
import './styles/css/styles.css';
import './styles/css/reset.css';
import './styles/css/loginStyle.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './styles/css/animate.min.css';
import './styles/css/font-awesome.min.css';
import HomePage from './components/HomePage.js';

function App() {

  return (  
    <div>
      <Nav/>
      <HomePage/>
    </div>  
  );
}

export default App;
