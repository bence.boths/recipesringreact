import { useState } from 'react';
import Nav from './Nav';
import { Navigate } from 'react-router-dom';

function Register() {

    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [passwordAgain, setPasswordAgain] = useState();

    const register = (event) => {
        event.preventDefault();
        console.log(username);
        if(password === passwordAgain) {
            const user = {username, password};
            fetch("http://localhost:8080/api/users/", {
                method: 'POST',
                mode: 'cors',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(user)
            })
            .then(() => {
                fetch("http://localhost:8080/api/users/name/" + username)
                .then(res => res.json())
                .then((res) => {
                    sessionStorage.setItem('token', String(res))
                    setUsername(2);
                })
            })
        }
    }

    if(sessionStorage.getItem('token')) {
        return (<Navigate to='/' />)
    }
    return(
        <div>
            <Nav />
            <div id="login-form-wrap">
                <h2>Registration</h2>
                <form id="login-form" onSubmit={register}>
                    <p>
                    <input type="username" placeholder="Username" required onChange={e => setUsername(e.target.value)}/><i className='validation'><span></span><span></span></i>
                    </p>
                    <p>
                    <input type="password" placeholder="Password" required onChange={e => setPassword(e.target.value)}/><i className='validation'><span></span><span></span></i>
                    </p>
                    <p>
                    <input type="password" placeholder="Password again" required onChange={e => setPasswordAgain(e.target.value)}/><i className='validation'><span></span><span></span></i>
                    </p>
                    <p>
                    <input type="submit" id="register" value="Register"/>
                    </p>
                </form>
            <div id="create-account-wrap"></div>
            </div>  
        </div>
    )
}

export default Register;