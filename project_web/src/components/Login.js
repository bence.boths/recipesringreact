import {useState} from 'react';
import Nav from './Nav';
import '../styles/css/styles.css';
import '../styles/css/reset.css';
import '../styles/css/loginStyle.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../styles/css/animate.min.css';
import '../styles/css/font-awesome.min.css';
import {  Navigate } from 'react-router-dom';

function Login() {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const loginSubmit = (e) => {
        e.preventDefault();
        const user = {username, password};
        fetch("http://localhost:8080/api/users/login", {
            method: 'POST',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(user)
        }).then(resp => resp.json())
        .then(resp => {
            if(resp) {
                // Logged succesfully
                fetch("http://localhost:8080/api/users/name/" + username)
                .then(res => res.json())
                .then((res) => {
                    sessionStorage.setItem('token', String(res))
                    setUsername(2);
                })
            }
        })
    };

    if(sessionStorage.getItem('token')) {
        return (<Navigate to='/' />)
    }
    return(
        <div>
            <Nav/>
            <div id="login-form-wrap">
                <h2>Login</h2>
                <form id="login-form" onSubmit={loginSubmit}>
                    <p>
                    <input type="username" placeholder="Username" required onChange={e => setUsername(e.target.value)}/><i className='validation'><span></span><span></span></i>
                    </p>
                    <p>
                    <input type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required/><i className='validation'><span></span><span></span></i>
                    </p>
                    <p>
                    <input type="submit" id="login" value="Login"/>
                    </p>
                </form>
                <div id="create-account-wrap">
                    <p>Not a member? <a href="/register">Create Account</a></p>
                </div>
            </div>
        </div>

    )
}

export default Login;