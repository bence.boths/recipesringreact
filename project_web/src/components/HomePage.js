import { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

function HomePage() {

    const [recipes, setRecipes] = useState();

    const getRecipes = () => {
        fetch('http://localhost:8080/api/recipes')
        .then(res => res.json())
        .then(data => setRecipes(data))
        .catch(err => {
            console.log(err);
        })
    }

    useEffect(() => {
        getRecipes();
    },[])

    const deleteIngredients = (rid) => {
        fetch('http://localhost:8080/api/ingredients/rid/' + rid, {
            method: 'DELETE',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'}        
        }).catch(error => {
            console.log(error);
        });
    }

    const deleteDirections = (rid) => {
        fetch('http://localhost:8080/api/directions/rid/' + rid, {
            method: 'DELETE',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'}        
        }).catch(error => {
            console.log(error);
        });
    }

    const deleteComments = (rid) => {
        fetch('http://localhost:8080/api/comments/rid/' + rid, {
            method: 'DELETE',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'}        
        }).catch(error => {
            console.log(error);
        });
    }

    const isUserRecipe = (id) => {
        return id == sessionStorage.getItem('token');
    }

    const deleteRecipe = (event, rid) => {
        event.preventDefault();
        deleteIngredients(rid);
        deleteDirections(rid);
        deleteComments(rid);
        fetch('http://localhost:8080/api/recipes/' + rid, {
            method: 'DELETE',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'}        
        }).then(() => {
            setRecipes(recipes.filter(item => item.rid !== rid));
        }).catch(error => {
            console.log(error);
        });
    }

    if(!sessionStorage.getItem('token')) {
        return(<Navigate to='/login'/>)
    }
    return(
        <div>
            <section id="logo">
                <div className='container text-center wow pulse'>
                    <br />
                    <h1>Recipe Book</h1>
                </div>
            </section>

            <section id="items">
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <h2>Recipes</h2>
                        </div>
                    </div>

                    <div className='row'>
                        {recipes && recipes.map((recipe) => (
                            <div className='col-lg-4 col-md-6 col-sm-12 wow fadeIn' key={recipe.rid}>
                                <div className='recipe-item text-center'>
                                    <a href={"/recipes/" + recipe.rid}>
                                        <img src={recipe.image} alt=""/>
                                    </a>
                                    <br />
                                    <h3>{recipe.name}</h3>
                                    { isUserRecipe(recipe.id) && 
                                    <a href='#' onClick={event => deleteRecipe(event, recipe.rid)}>
                                        <i className='fa fa-trash' style={{fontSize:22 + "px"}}></i>&emsp;
                                    </a>}
                                    { isUserRecipe(recipe.id) && 
                                    <a href='/update'>
                                        <i className='fa fa-edit' style={{fontSize:22 + "px"}}></i>
                                    </a> }
                                </div>
                            </div> 
                         ))}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default HomePage;