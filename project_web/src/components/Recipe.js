import Nav from './Nav';
import '../styles/css/styles.css';
import '../styles/css/reset.css';
import '../styles/css/loginStyle.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../styles/css/animate.min.css';
import '../styles/css/font-awesome.min.css';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function Recipe() {

    const [recipe, setRecipe] = useState(0);
    const [ingredients, setIngredients] = useState();
    const [directions, setDirections] = useState();
    const [comments, setComments] = useState();
    const [text, setText] = useState();
    const [newComment, setNewComment] = useState();

    const rid = useParams().id;

    const isUserRecipe = (id) => {
        return id == sessionStorage.getItem('token');
    }

    const deleteComment = (event,cid) => {
        event.preventDefault();
         fetch(`http://localhost:8080/api/comments/${cid}`,{
            method: 'DELETE',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'},
         }).then(() => {
           setNewComment(cid); 
         })
         .catch(error =>
            console.log(error));
    }

    const addComment = (event) => {
        event.preventDefault();
        const id = sessionStorage.getItem('token');
        const rid = recipe.rid;
        const current = new Date();
        const date = `${current.getFullYear()}-${current.getMonth()+1}-${current.getDate()}-`;
        const newComment = {text, date, id, rid};
        fetch(`http://localhost:8080/api/comments`, {
            method: 'POST',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'},
            body : JSON.stringify(newComment)
        }).then(() => {
            setNewComment(newComment)
        })
        .catch(error => {
            console.log(error);
        })
    }

    let getDetailedRecipe = (rid) => {
        fetch(`http://localhost:8080/api/recipes/${rid}`)
        .then((res) => res.json())
        .then((resp) => setRecipe(resp))
    }

    const getIngredients = (rid) => {
        fetch('http://localhost:8080/api/ingredients/' + rid)
        .then(res => res.json())
        .then(resp => setIngredients(resp))
    }

    const getDirections = (rid) => {
        fetch('http://localhost:8080/api/directions/' + rid)
        .then(res => res.json())
        .then(resp => setDirections(resp))
    }
    const getComments = (rid) => {
        fetch('http://localhost:8080/api/comments/rid/' + rid)
        .then(res => res.json())
        .then(resp => setComments(resp))
    }

    useEffect(() => {
        getDetailedRecipe(rid);
        getIngredients(rid);
        getDirections(rid);
        getComments(rid);
    }, [])

    useEffect(() => {
        getComments(rid);
        setText('');
    }, [newComment])


    if(recipe === 0) {
        return(<div>Loading..</div>)
    }
    return(
        <div>
            <Nav />
            <section id="logo">
                <div className="container text-center wow pulse">
                    <img src="/images/logo-white.svg" alt="logo" />
                    <br />
                    <h1>Recipe Book</h1>
                </div>
            </section>

            <section id="recipe">
                <div className="container">
                <div className="row">
                    {/* <!-- Title --> */}
                    <div className="col-12">
                    <h2> {recipe && recipe.name}</h2>
                    </div>
                </div>
                <div className="row vertical-align">
                    <div className="col-12">
                    {/* <!-- Picture --> */}
                    <div className="col-md-8 pull-left wow swing">
                        <img src={recipe && "/"+recipe.image} alt="kep neve" className="recipe-picture" />
                    </div>
                    {/* <!-- Info --> */}
                    <div className="col-md-4 pull-right wow lightSpeedIn">
                        <div className="recipe-info">
                        <h3>Info</h3>
                        {/* <!-- Time --> */}
                        <div className="row">
                            <div className="col-2 text-center">
                            <i className="fa fa-clock-o" aria-hidden="true"></i>
                            </div>
                            <div className="col-6">Time</div>
                            <div className="col-4">{recipe && recipe.time}</div>
                        </div>
                        {/* <!-- Difficulty --> */}
                        <div className="row">
                            <div className="col-2 text-center">
                            <i className="fa fa-area-chart" aria-hidden="true"></i>
                            </div>
                            <div className="col-6">Difficulty</div>
                            <div className="col-4">{recipe && recipe.difficulty}</div>
                        </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                {/* <!-- Ingredients --> */}
                <div className="row wow slideInUp">
                    <div className="col-12">
                        <div className="recipe-ingredients">
                            <h3>Ingredients</h3>
                            <dl className="ingredients-list">
                                {ingredients && ingredients.map(ing => (
                                    <span key={ing.iid}>
                                    <dt>{ing.quantity}</dt> 
                                    <dd>{ing.name} </dd>
                                    </span>
                                ))}   
                            </dl>   
                        </div>
                    </div>
                </div>
                {/* <!-- Directions --> */}
                <div className="row wow slideInUp">
                    <div className="col-12">
                    <div className="recipe-directions">
                        <h3>Directions</h3>
                        <ol>
                            { directions && directions.map(dir => (
                               <li key={dir.did}>{dir.description}</li>
                            ))}
                        </ol>
                    </div>
                    </div>
                </div>

            {/* <!-- Comment box */}
            {/* ===================== --> */}
            <div className="container mt-5">
                <div className="d-flex justify-content-center row">
                    <div className="col-md-8">
                    <div className="d-flex flex-column comment-section">
                        <div className="bg-white p-2">
                        {comments && comments.map(comment =>(
                            <span key={comment.cid}>
                            <div className="d-flex flex-row user-info">
                             <div className="d-flex flex-column justify-content-start ml-2">
                                <span className="d-block font-weight-bold name">{comment.authorName}</span>
                                <span className="date text-black-50">{comment.date}</span>
                            
                            {isUserRecipe(comment.id) && <a href='#' onClick={event => deleteComment(event, comment.cid)}>
                                <i className="fa fa-trash" style= {{fontSize: 22 + "px"}}></i>&emsp;
                             </a>}
                             </div>
                            </div>
                        <div className="mt-2">
                            <p className="comment-text">{comment.text}</p>
                        </div>
                        <hr/>
                        </span>
                        ))}
                        </div>
                        <div className="bg-light p-2">
                        <div className="d-flex flex-row align-items-start">
                            <form onSubmit={addComment}>
                            <textarea rows = "5" cols = "50" value={text} onChange={e => {setText(e.target.value)}}></textarea>
                            <p>
                                <input type="submit" id="submit" value="Comment"/>
                            </p>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                {/* <!-- Back to recipes --> */}
                <div className="row wow rollIn">
                <div className="col-12 text-center">
                    <a href="/"><br/>Go to back to recipes.</a>
                </div>
                </div>
            </section>
        </div>
    )
}

export default Recipe;