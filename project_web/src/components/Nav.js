import { useState } from "react";
import { Navigate } from "react-router-dom";

function Nav(){

    const [logged, setLogged] = useState(true);

    const logout = () => {
        setLogged(false);
        sessionStorage.clear();
    }

    if(!logged) {
        return <Navigate to='/login' />
    }

    return(
        <div className="topnav">
            <a href="/">Home</a>
            <a href="/upload">Add recipe</a>
            <a href="#" onClick={logout}>Logout</a>
        </div>
    )
}

export default Nav;