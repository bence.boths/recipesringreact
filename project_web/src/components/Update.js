import Nav from "./Nav";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function Update() {

    const [name, setName] = useState();
    const [time, setTime] = useState();
    const [imageData, setImageData] = useState();
    const [imagePreview, setImagePreview] = useState();
    const [imageName, setImageName] = useState();
    const [difficulty, setDifficulty] = useState();
    const [ingredients, setIngredients] = useState();
    const [directions, setDirections] = useState();
    const [recipe, setRecipe] = useState();
    const rid = useParams().id;


    const update = () => {

    }

    let getDetailedRecipe = (rid) => {
        fetch(`http://localhost:8080/api/recipes/${rid}`)
        .then((res) => res.json())
        .then((resp) => setRecipe(resp))
    }

    const getIngredients = (rid) => {
        fetch('http://localhost:8080/api/ingredients/' + rid)
        .then(res => res.json())
        .then(resp => setIngredients(resp))
    }

    const getDirections = (rid) => {
        fetch('http://localhost:8080/api/directions/' + rid)
        .then(res => res.json())
        .then(resp => setDirections(resp))
    }

    const handleUploadClick = event => {
        let file = event.target.files[0];
        const imageData = new FormData();
        imageData.append('imageFile', file);
        setImageData(imageData);
        setImageName(file.name);
        setImagePreview(URL.createObjectURL(file));
    };

    useEffect(() => {
        getDetailedRecipe(rid);
        getIngredients(rid);
        getDirections(rid);
    }, [])
    
    return(
        <div>
            <Nav />
            <div id="login-form-wrap">
                <h2>Upload</h2>
                <form id="login-form" onSubmit={update} encType="multipart/form-data">
                    <p>
                        <input type="username" placeholder={recipe.name} required onChange={e => {setName(e.target.value)}}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder={recipe.time} required onChange={e => {setTime(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder={recipe.difficulty} required onChange={e => {setDifficulty(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="" required onChange={e => {setIngredients(e.target.value)}}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="direction1;direction2..." required onChange={e => {setDirections(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="file" required onChange={handleUploadClick}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="submit" value="Upload"/>
                    </p>
                </form>
                <div id="create-account-wrap"></div>
            </div>
        </div>
    )
}

export default Update;