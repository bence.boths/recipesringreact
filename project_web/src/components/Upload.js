import { useState } from "react";
import Nav from "./Nav";
import axios from 'axios';
import { Navigate } from "react-router-dom";

function Upload() {

    const [name, setName] = useState();
    const [time, setTime] = useState();
    const [imageData, setImageData] = useState();
    const [imagePreview, setImagePreview] = useState();
    const [imageName, setImageName] = useState();
    const [difficulty, setDifficulty] = useState();
    const [ingredients, setIngredients] = useState();
    const [directions, setDirections] = useState();
    const [newRecipeId, setNewRecipeId] = useState();

    const uploadRecipe = (recipe) => {
        fetch('http://localhost:8080/api/recipes/', {
            method: 'POST',
            mode: "cors",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(recipe)
        }).then(res => res.json())
        .then(resp => setNewRecipeId(resp.rid+1))
        .then(console.log(newRecipeId))
        .catch(error => {
            console.log(error);
        })
    }

    const uploadIngredients = () => {
        let splittedIngredients = ingredients.split(';');
        for(let i = 0; i < splittedIngredients.length; i++) {
            const tmp = splittedIngredients[i].split(',');
            const ing = tmp[0];
            const count = tmp[1];
            let newIngredients = {"name": ing, "quantity": count, "rid": newRecipeId};
            fetch('http://localhost:8080/api/ingredients/', {
                method: 'POST',
                mode: "cors",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(newIngredients)
            }).catch(error => console.log(error))
        }
    }

    const uploadDirections = () => {
        const splittedDirections = directions.split(';');
        for(let i = 0; i < splittedDirections.length; i++) {
            const dir = splittedDirections[i];
            const newDirection = {"description": dir, "rid": newRecipeId};
            fetch('http://localhost:8080/api/directions/', {
                method: 'POST',
                mode: "cors",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(newDirection)
            })
        }
    }

    const upload = (event) => {
        event.preventDefault();
        const image = 'images/' + imageName;
        const id = sessionStorage.getItem('token');
        const recipe = {name, image, time, difficulty, id};
        uploadRecipe(recipe);
        console.log(newRecipeId);
        uploadIngredients();
        uploadDirections();
        setDirections(0);
    }
    
    const handleUploadClick = event => {
        let file = event.target.files[0];
        const imageData = new FormData();
        imageData.append('imageFile', file);
        setImageData(imageData);
        setImageName(file.name);
        setImagePreview(URL.createObjectURL(file));
    };

    if(directions === 0) {
        return <Navigate to='/'/>;
    }

    return(
        <div>
            <Nav />
            <div id="login-form-wrap">
                <h2>Upload</h2>
                <form id="login-form" onSubmit={upload} encType="multipart/form-data">
                    <p>
                        <input type="username" placeholder="Name" required onChange={e => {setName(e.target.value)}}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="Time" required onChange={e => {setTime(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="Difficulty" required onChange={e => {setDifficulty(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="ingredient1,quantity1;ingredient2,quantity2... " required onChange={e => {setIngredients(e.target.value)}}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="username" placeholder="direction1;direction2..." required onChange={e => {setDirections(e.target.value)}} /><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="file" required onChange={handleUploadClick}/><i className="validation"><span></span><span></span></i>
                    </p>
                    <p>
                        <input type="submit" value="Upload"/>
                    </p>
                </form>
                <div id="create-account-wrap"></div>
            </div>
        </div>
    )
}

export default Upload;